# Generated by Django 2.2 on 2019-05-24 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='User_typ',
            field=models.CharField(choices=[('Author', 'Author'), ('Normal user', 'Normal user')], default='Anonymous', max_length=100),
        ),
    ]
