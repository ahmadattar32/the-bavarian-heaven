# Generated by Django 2.2 on 2019-06-21 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20190524_1654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='Class',
            field=models.CharField(choices=[('Administrator', 'Administrator'), ('Author', 'Author'), ('Normal user', 'Normal user')], default='Anonymous', max_length=100),
        ),
    ]
