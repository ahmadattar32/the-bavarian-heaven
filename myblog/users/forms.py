from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
    user_choices = (
        ('Administrator', 'Administrator'),
        ('Author', 'Author'),
        ('Normal user', 'Normal user'),
    )
    Class = forms.ChoiceField(choices=user_choices)
    class Meta:
        model = Profile
        fields = ['Class', 'image']