from django.test import TestCase, Client
from django.urls import reverse, resolve

class Test_views(TestCase):
    def setUp(self):
        self.client = Client()
        self.register_url = reverse('register')


    def test_project_register_GET(self):
        response = self.client.get(self.register_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/registration.html')
