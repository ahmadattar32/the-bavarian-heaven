from django.test import TestCase
from users.forms import UserUpdateForm




class TestForms(TestCase):

    def test_UserUpdateForm_form_valid_data(self):
        form = UserUpdateForm(data={
            'email': 'Mouadmass@gmail.com',
            'username': 'Mouad717'
        })
        self.assertTrue(form.is_valid())

    def test_UserUpdateForm_form_no_data(self):
        form = UserUpdateForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
