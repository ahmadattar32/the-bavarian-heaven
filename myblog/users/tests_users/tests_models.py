from django.test import TestCase
from users.models import Profile
from django.contrib.auth.models import User
from mixer.backend.django import mixer


class TestModels(TestCase):

    def setUp(self):
        user = User.objects.create(username='majdi', email='attar@gmail.com')
        profile = mixer.blend('users.Profile', user=user, Class='Normal user', id=1)

    def test_Profile_user(self):
            profile = Profile.objects.get(id=1)
            username = profile.user.username
            self.assertEquals(username, 'majdi')

    def test_Profile_email(self):
            profile = Profile.objects.get(id=1)
            email = profile.user.email
            self.assertEquals(email, 'attar@gmail.com')

    def test_Profile_Class(self):
            profile = Profile.objects.get(id=1)
            Class = profile.Class
            self.assertEqual(Class, 'Normal user')
