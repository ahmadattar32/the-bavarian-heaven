from django.urls import reverse, resolve
from django.test import TestCase
from blog.views import home, about, calender


class AboutTest(TestCase):

    def test_root_url_resolves_to_about_page_view(self):
        path = reverse('About')
        found = resolve(path)
        self.assertEqual(found.func, about)

    def test_root_url_resolves_to_home_page_view(self):
        path = reverse('home-page')
        found = resolve(path)
        self.assertEqual(found.func, home)

    def test_root_url_resolves_to_calender_view(self):
        path = reverse('Calender')
        found = resolve(path)
        self.assertEqual(found.func, calender)
