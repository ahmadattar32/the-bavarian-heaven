
from django.test import TestCase
from blog.models import Post
from mixer.backend.django import mixer


class PostTests(TestCase):

    def setUp(self):
        post = mixer.blend('blog.Post', title='title1', content='just a test')

    def test_content_content(self):
        post1 = Post.objects.get(pk=1)
        first_content = post1.content
        self.assertEquals(first_content, 'just a test')

    def test_title_content(self):
        post = Post.objects.get(id=1)
        first_title = post.title
        self.assertEquals(first_title, 'title1')

    def test_get_absolute_url(self):
        post = Post.objects.get(id=1)
        self.assertEquals(post.get_absolute_url(), '/post/1/')












