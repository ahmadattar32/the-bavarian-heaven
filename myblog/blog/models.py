from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image


class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='post_image')
    tag_choices = (
        ('Sport', 'Sport'),
        ('Restaurants', 'Restaurants'),
        ('Historical Landmarks', 'Historical Landmarks'),
        ('Divers', 'Divers'),
    )
    tag = models.CharField(max_length=100, default='non specified', choices=tag_choices)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail',args=[self.pk])

    def save(self):
        super().save()

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)
    class Meta:
        ordering = ('-date_posted', )

class Event(models.Model):
    title = models.CharField(max_length=200)
    location = models.CharField(max_length=200, default='non specified')
    description = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    author = models.CharField(max_length=200)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('event-detail', args=[self.pk])

    def save(self):
        super().save()

class Comment(models.Model):
    post = models.ForeignKey('blog.Post', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text