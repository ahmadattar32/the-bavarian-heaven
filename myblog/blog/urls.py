from django.urls import path
from .views import (
    PostDetailView,
    PostCreateView,
    PostUpdateView,
    PostDeleteView,
    EventDetailView,
    EventCreateView,
    EventUpdateView,
    EventDeleteView,
    CommentDeleteView,
)
from . import views

urlpatterns = [
    path('', views.home, name='home-page'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('post/new/', PostCreateView.as_view(), name='post-create'),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    path('about/', views.about, name='About'),
    path('archive/', views.archive, name='Archive'),
    path('archive/descending', views.descendind, name='date-descending'),
    path('archive/Sport', views.Sport, name='Sport'),
    path('archive/Restaurants', views.Restaurants, name='Restaurants'),
    path('archive/Landmarks', views.Landmarks, name='Landmarks'),
    path('archive/Divers', views.Divers, name='Divers'),
    path('calender/', views.calender, name='Calender'),
    path('event/<int:pk>/', EventDetailView.as_view(), name='event-detail'),
    path('event/new/', EventCreateView.as_view(), name='event-create'),
    path('event/<int:pk>/update/', EventUpdateView.as_view(), name='event-update'),
    path('event/<int:pk>/delete/', EventDeleteView.as_view(), name='event-delete'),
    path('post/<int:pk>/comment/', views.add_comment_to_post, name='add_comment_to_post'),
    path('post/<int:id>/comment/<int:pk>/approve/', views.comment_approve, name='comment_approve'),
    path('post/<int:id>/comment/<int:pk>/remove/', CommentDeleteView.as_view(), name='comment_remove'),

]